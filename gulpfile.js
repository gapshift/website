// Gulp file

var gulp = require('gulp');
var uglify = require('gulp-uglify');
var gulpIf = require('gulp-if');
var rename = require('gulp-rename');
var sass = require('gulp-sass');
var cssnano = require('gulp-cssnano');

gulp.task('watch', function(){
  gulp.watch('src/js/*.js', ['uglify']);
	gulp.watch('src/sass/*.scss', ['sass']);
	gulp.watch('src/sass/components/*.scss', ['sass']);
	gulp.watch('src/sass/mdl/*.scss', ['sass']);
  // Other watchers
});

gulp.task('uglify', function(){
	return gulp.src('src/js/*.js')
		.pipe(gulpIf('*.js', uglify()))
		.pipe(rename({ suffix: '.min' }))
    .pipe(gulp.dest('public/assets/js'));
});

gulp.task('sass', function(){
	return gulp.src('src/sass/*.scss')
		.pipe(gulpIf('*.scss', sass()))
		.pipe(gulpIf('*.css', cssnano()))
		.pipe(rename({ suffix: '.min' }))
    .pipe(gulp.dest('public/assets/css'));
});

import React, { Component } from 'react';
import { Helmet } from 'react-helmet';
import Assets from '../constants/Assets';
import { Section } from '../components';

class NotFound extends Component {
  render() {
    return (
      <div>
        <Helmet>
          <title>Page not found - Gapshift</title>
          <meta name="description" content="Oops! We can't seem to find the page you're looking for." />
        </Helmet>

        {/* Page title and form */}
        <Section class="container-fluid">
          <div className="container">
        		<div className="row">
              <div className="col s12 m5 push-m7 center-align">
                <div className="vert-spacer h10 hide-on-small-only" />
                <div className="vert-spacer h10 hide-on-small-only" />
                <img src={Assets.cookieCrumbsImg} width="80%" alt="Cookie crumbs" />
              </div>
        			<div className="col s12 m7 pull-m5">
                <h1 className="shadow dull-black-text xl-text">Oops!</h1>
                <h3>We can't seem to find the page you're looking for.</h3>
                <p>Error code: 404</p>
                <p>Here are some helpful links instead:</p>
                <ul className="browserDefaults">
                  <li><a href="/">Home</a></li>
                  <li><a href="/#how-we-work">How we work</a></li>
                  <li><a href="/newproject">Start a new project</a></li>
                  <li><a href="/contact">Contact us</a></li>
                </ul>
              </div>

            </div>
          </div>
        </Section>
      </div>
    );
  }
}

export default NotFound;

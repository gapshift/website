import React, { Component } from 'react';
import { Helmet } from 'react-helmet';
import { Section, Curly, LegendBenefits } from '../components';
import Assets from '../constants/Assets';

class CreativeLegends extends Component {
  render() {
    return (
      <div>
        <Helmet>
          <title>Calling Creative Legends - Gapshift</title>
          <meta name="description" content="We are looking for extraordinary talent to join our network of freelancers. If you consider yourself a developer ninja, animation jedi, designer rockstar, social media wizard or any other creative legend, we would like to know." />
          <meta name="twitter:card" content="summary_large_image" />
          <meta name="twitter:site" content="@gapshift" />
          <meta name="twitter:creator" content="@gapshift" />
          <meta name="twitter:title" content="Calling Creative Legends" />
          <meta name="twitter:description" content="If you consider yourself a creative legend, we would like to know. Sign up" />
          <meta name="twitter:image" content={Assets.twitter.cards.creativeLegends} />
        </Helmet>

        {/* Page title and form */}
        <Section class="container-fluid">
          <div className="container">
        		<div className="row page-title">
        			<div className="col s12">
                <div className="vert-spacer h8 hide-on-med-and-down"></div>
        				<h1 className="shadow dull-black-text">Calling Creative Legends</h1>
        				<Curly black />
        			</div>
        		</div>
        	</div>
        </Section>

        {/* Graphic section */}
        <Section class="container-fluid dark-purple-bg no-padding">
          <div className="legends-header">
            <img src={Assets.legendsHeader} alt="" />
          </div>
        </Section>

        {/* Intro section */}
        <Section class="container-fluid electric-blue-bg">
        	<div className="container">
            <div className="row">
              <div className="col s12 black-text center-align">
                <h2 className="black-text">Do you have that legendary uniqueness in you?</h2>
                <Curly black />
                <p>We’re looking for extraordinary talent to join our network of freelancers. If you consider yourself a developer ninja, animation jedi, designer rockstar, social media wizard or any other creative legend, we would like to know.</p>
              </div>
            </div>
        	</div>
        </Section>

        {/* Benefits section */}
        <Section class="container-fluid lush-purple-bg">
        	<div className="container">
            <div className="row">
              <div className="col s12 white-text center-align">
                <h2 className="white-text">Benefits of working with us</h2>
                <Curly />
                <div className="vert-spacer h6"></div>
                <LegendBenefits />
              </div>
            </div>
        	</div>
        </Section>

        {/* Form section */}
        <Section class="container-fluid">
        	<div className="container">
            <div className="row">
              <div className="col s12 center-align">
                <h2 className="black-text">Interested?</h2>
                <Curly dark />
                <p>Please supply us with the following information</p>
              </div>
            </div>
            <div className="row">
              <form id="creative-legends-form" className="col s12 m6 offset-m3" noValidate>
                <div className="col s12">
                  <label htmlFor="legend_name">Name*</label>
                  <input type="text" id="legend_name" name="legend_name" placeholder="Name" pattern=".{2,}" required />
                </div>
                <div className="col s12">
                  <label htmlFor="legend_surname">Surname*</label>
                  <input type="text" id="legend_surname" name="legend_surname" placeholder="Surname" pattern=".{2,}" required />
                </div>
                <div className="col s12">
                  <label htmlFor="legend_email">Email*</label>
                  <input type="email" id="legend_email" name="legend_email" placeholder="Email" pattern="[a-zA-Z0-9._%+-]{1,}@[a-z0-9.-_]{1,}\.[a-z]{2,63}$" required />
                </div>
                <div className="col s12">
                  <label htmlFor="legend_number">Mobile number</label>
                  <input type="tel" id="legend_number" name="legend_number" placeholder="e.g. 084 123 4567" pattern="[0-9]{2,3}[\s]{0,1}[0-9]{3}[\s]{0,1}[0-9]{4}$" />
                </div>
                <div className="col s12">
                  <label htmlFor="legend_bio">Short bio*</label>
                  <textarea id="legend_bio" name="legend_bio" placeholder="Short bio of yourself" required></textarea>
                </div>
                <div className="col s12">
                  <label htmlFor="legend_portfolio">Portfolio link*</label>
                  <input type="text" id="legend_portfolio" name="legend_portfolio" placeholder="Portfolio link" required />
                </div>
                <div className="col s12">
                  <label htmlFor="legend_experience">Years experience*</label>
                  <select id="legend_experience" name="legend_experience" className="browser-default" required>
                    <option value="1-3">1-3 years</option>
                    <option value="3-5">3-5 years</option>
                    <option value="5+">5+</option>
                  </select>
                </div>
                <div className="col s12">
                  <label htmlFor="legend_refs">References*</label>
                  <textarea id="legend_refs" name="legend_refs" placeholder="Supply 3 references incl. contact name, company name, email & contact number" required></textarea>
                </div>
                <div className="col s12">
                  <input type="text" id="bots" className="bots-honeypot" />
                  <button type="submit" className="waves-effect waves-dark btn-large moon-blue-btn">Send</button>
                </div>
            </form>
            </div>
        	</div>
        </Section>
      </div>
    );
  }
}

export default CreativeLegends;

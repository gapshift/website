import React from 'react';
import { Helmet } from 'react-helmet';
import Assets from '../constants/Assets';
import { Section, Steps, ClientCarItem, Projects, Services, Modals, Curly, HeroSection } from '../components';

export default class HomePage extends React.Component {
  render() {
    return (
      <div>
        <Helmet>
          <title>An unusual, no boundaries digital bakery - Gapshift</title>
          <meta name="description" content="Smell the difference with an unusual, no boundaries digital bakery. Gapshift are a diverse and handpicked network of creative legends, offering both brands and agencies a scalable solution that helps them expand on demand." />
          <meta name="twitter:card" content="summary_large_image" />
          <meta name="twitter:site" content="@gapshift" />
          <meta name="twitter:creator" content="@gapshift" />
          <meta name="twitter:title" content="An on-demand digital bakery" />
          <meta name="twitter:description" content="Need resources? We deliver tasteful digital goodness" />
          <meta name="twitter:image" content={Assets.twitter.cards.default} />
        </Helmet>

        {/* Hero section */}
        <HeroSection />

        {/* Intro section */}
        <Section id="intro" className="container scrollspy">
        	<div className="row">
        		<div className="col s12 scrollspy">
        			<h1 className="shadow">Smell the difference</h1>
        			<Curly dark />
        			<p>At Gapshift we believe that a “gap” is a time or a set of circumstances that makes it possible to do something that breaks the habit of a conventional mindset. We get a curious sensory experience from all things creative, stretching our capabilities beyond boundaries to be distinctive “gap-shifters”. We create and deliver innovative, revolutionary and tasteful digital goodness.</p>
							<p>We are a diverse and handpicked network of creative legends, offering both brands and agencies a scalable solution that helps them expand to meet their immediate demand, without the overheads of onboarding permanent/temporary staff.</p>
							<div className="vert-spacer h2"></div>
							<h3>We bake unusually fresh <span className="ticker"></span><span className="ticker-cursor">|</span></h3>
        			<div className="vert-spacer h3"></div>
        			<a href="/newproject" className="waves-effect waves-dark btn-large aqua-green-btn">get started</a>
        			<div className="vert-spacer h1 hide-on-med-and-up"></div>
        			<a href="#how-we-work" className="waves-effect waves-dark btn-large grey-btn">how we work</a>
        		</div>
        	</div>
        </Section>

        {/* How it works section */}
        <Section id="how-we-work" className="container-fluid chirpy-purple-bg scrollspy">
        	<div className="container">
        		<div className="row">
        			<div className="col s12 dull-black-text center-align">
        				<h2 className="white-text">How we work</h2>
        				<Curly />
        				<div className="vert-spacer h6"></div>
        				<div className="timeline">
        					<div className="line"></div>
        					<Steps />
        				</div>
        			</div>
        		</div>
        	</div>
        </Section>

        {/* Services section */}
        <Section id="services" className="container-fluid dark-purple-bg scrollspy">
        	<div className="container">
        		<div className="row">
        			<div className="col s12 white-text center-align">
        				<h2 className="white-text">Our services</h2>
        				<Curly />
        				<div className="vert-spacer h6"></div>

                <Services />

                <div className="vert-spacer h6"></div>

        				<a href="/newproject" className="waves-effect waves-dark btn-large moon-blue-btn">Start a new project</a>

        			</div>
        		</div>
        	</div>
        </Section>

        {/* Our work section */}
        <Section id="work" className="container-fluid aqua-green-bg scrollspy">
        	<div className="container-widebox">
        		<div className="row">
        			<div className="col s12 white-text center-align">
        				<h2 className="white-text">Some of our past and recent work</h2>
        				<Curly />
        				<div className="vert-spacer h2"></div>
        			</div>

        			<div className="vert-spacer h4"></div>

        			<div className="col s12 center-align">
        				<ul className="tabs aqua-green-bg">
        					<li className="tab"><a className="active" data-target="all">All</a></li>
        					<li className="tab"><a className="" data-target="web">Web development</a></li>
        					<li className="tab"><a className="" data-target="motion">Motion Graphics</a></li>
        				</ul>
        			</div>

        			<div className="vert-spacer h4"></div>

        			<Projects />

        		</div>
        	</div>
        </Section>

        {/* Cients carousel section */}
        <Section id="clients" className="container-fluid less-padding valign-wrapper">
        	<div className="owl-carousel">
            <ClientCarItem image={Assets.clients.bcx} alt="BCX" />
            <ClientCarItem image={Assets.clients.mtn} alt="MTN" />
            <ClientCarItem image={Assets.clients.ogilvy} alt="Ogilvy" />
            <ClientCarItem image={Assets.clients.pfs} alt="Peaceful Sleep" />
            <ClientCarItem image={Assets.clients.pindrop} alt="Pindrop" />
            <ClientCarItem image={Assets.clients.standardBank} alt="Standard Bank" />
            <ClientCarItem image={Assets.clients.tbwa} alt="TBWA" />
            <ClientCarItem image={Assets.clients.vodacom} alt="Vodacom" />
            <ClientCarItem image={Assets.clients.absa} alt="Absa" />
        	</div>
        </Section>

        {/* CTA section */}
        <Section id="cta" className="container-fluid candy-pink-bg valign-wrapper">
        	<div className="container">
        		<div className="row">
        			<h1 className="shadow white-text thin-text">Think we'd be a match?</h1>
        			<div className="vert-spacer h4"></div>
        			<a href="/newproject" className="waves-effect waves-dark btn-large aqua-green-btn">Let's start baking</a>
        		</div>
        	</div>
        </Section>

        {/* Modals */}
        <Modals />
      </div>
    );
  }
}

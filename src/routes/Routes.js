import React, { Component } from 'react';
import { Route, Switch } from "react-router-dom";
import { HomePage, ContactPage, NewProjectPage, PrivacyPolicyPage, NotFound, CreativeLegends } from './'

class Routes extends Component {
  render() {
    return (
      <Switch>
        <Route exact path="/" component={HomePage} />
        <Route path="/contact" component={ContactPage} />
        <Route path="/newproject" component={NewProjectPage} />
        <Route path="/privacy" component={PrivacyPolicyPage} />
        <Route path="/creative-legends" component={CreativeLegends} />
        <Route component={NotFound} />
      </Switch>
    );
  }
}

export default Routes;

import React, { Component } from 'react';
import { Helmet } from 'react-helmet';

import { Section, Accordion, Curly } from '../components';

class PrivacyPolicyPage extends Component {
  render() {
    return (
      <div>
        <Helmet>
          <title>Privacy policy - Gapshift</title>
          <meta name="description" content="This Privacy Policy explains how we collect, use, process, and disclose your information on our website." />
        </Helmet>

        {/* Page title and form */}
        <Section class="container-fluid">
          <div className="container">
        		<div className="row page-title">
        			<div className="col s12">
                <div className="vert-spacer h8 hide-on-med-and-down"></div>
        				<h1 className="shadow dull-black-text">Privacy Policy</h1>
                <h5>This Privacy Policy explains how we collect, use, process, and disclose your information on our website.</h5>
        				<Curly dark />
        			</div>
        		</div>
            <div className="row">
              <div className="col s12">
                <p><b>Last Updated: </b>10 December 2017</p>
                <p><b>Thank you for using Gapshift!</b></p>
                <p>This privacy policy has been compiled to better serve those who are concerned with how their 'Personally Identifiable Information' (PII) is being collected, used online, stored, shared and protected. PII is information that can be used on its own or with other information to identify, contact, or locate a single person, or to identify an individual in context. Please read our privacy policy carefully to get a clear understanding of how we collect, use, protect or otherwise handle your Personally Identifiable Information in accordance with our website.</p>
                <Accordion />
              </div>
            </div>
          </div>
        </Section>
      </div>
    );
  }
}

export default PrivacyPolicyPage;

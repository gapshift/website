import HomePage from './HomePage';
import ContactPage from './ContactPage';
import NewProjectPage from './NewProjectPage';
import PrivacyPolicyPage from './PrivacyPolicyPage';
import NotFound from './NotFound';
import CreativeLegends from './CreativeLegends';

export {
    HomePage,
    ContactPage,
    NewProjectPage,
    PrivacyPolicyPage,
    NotFound,
    CreativeLegends,
}
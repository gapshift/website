import React, { Component } from 'react';
import { Helmet } from 'react-helmet';
import { Section, Curly } from '../components';

class ContactPage extends Component {
  render() {
    return (
      <div>
        <Helmet>
          <title>Get in touch - Gapshift</title>
          <meta name="description" content="If you have any questions, enquiries or just want to say how much you like us (or even what we can do better), please drop us a line below. We would love to hear from you." />
        </Helmet>

        {/* Page title and form */}
        <Section class="container-fluid">
          <div className="container">
        		<div className="row page-title">
        			<div className="col s12">
                <div className="vert-spacer h8 hide-on-med-and-down"></div>
        				<h1 className="shadow dull-black-text">Get in touch</h1>
                <h5>If you have any questions, enquiries or just want to say how much you like us (or what we can do better), please drop us a line below. We would love to hear from you.</h5>
        				<Curly dark />
        			</div>
        		</div>
            <div className="row">
              <form id="contact-form" className="col s12 m8" noValidate>
          			<div className="col s12 m6">
                  <label htmlFor="contact_name">Name*</label>
          				<input type="text" id="contact_name" name="contact_name" placeholder="Name" pattern=".{2,}" required />
          			</div>
          			<div className="col s12 m6">
                  <label htmlFor="contact_surname">Surname*</label>
          				<input type="text" id="contact_surname" name="contact_surname" placeholder="Surname" pattern=".{2,}" required />
          			</div>
          			<div className="col s12">
                  <label htmlFor="contact_email">Email*</label>
          				<input type="email" id="contact_email" name="contact_email" placeholder="Email" pattern="[a-zA-Z0-9._%+-]{1,}@[a-z0-9.-_]{1,}\.[a-z]{2,63}$" required />
          			</div>
          			<div className="col s12 m6">
                  <label htmlFor="contact_number">Contact number</label>
          				<input type="tel" id="contact_number" name="contact_number" placeholder="e.g. 084 123 4567" pattern="[0-9]{2,3}[\s]{0,1}[0-9]{3}[\s]{0,1}[0-9]{4}$" />
          			</div>
                <div className="col s12">
                  <label htmlFor="contact_message">Message*</label>
          				<textarea id="contact_message" name="contact_message" placeholder="Leave us your message" required></textarea>
          			</div>
                <div className="col s12">
                  <input type="text" id="bots" className="bots-honeypot" />
      						<button type="submit" className="waves-effect waves-dark btn-large moon-blue-btn">Send</button>
      					</div>
                <div className="vert-spacer h5" />
              </form>

              <div className="col s12">
              <Curly dark />
                <p>Need a more personal touch? You can reach us at the details below.</p>
                <p>
                  <b>Contact number:</b> +27 (84) 207-5542
                </p>
                <p>
                  <b>Enquiries:</b> <a href="mailto:more@gapshift.com">more@gapshift.com</a><br />
                  <b>Billing:</b> <a href="mailto:billing@gapshift.com">billing@gapshift.com</a><br />
                  <b>Project support:</b> <a href="mailto:projects@gapshift.com">projects@gapshift.com</a>
                </p>
              </div>
        		</div>
        	</div>
        </Section>

        {/* CTA section */}
        <Section class="container-fluid candy-pink-bg valign-wrapper">
        	<div className="container">
        		<div className="row">
        			<h3 className="shadow white-text thin-text">Looking to start a new project instead?</h3>
        			<a href="/newproject" className="waves-effect waves-dark btn-large aqua-green-btn">Let's start baking</a>
        		</div>
        	</div>
        </Section>
      </div>
    );
  }
}

export default ContactPage;

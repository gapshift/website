import React, { Component } from 'react';
import { Helmet } from 'react-helmet';

import { Section, Curly } from '../components';

class NewProjectPage extends Component {
  render() {
    return (
      <div>
        <Helmet>
          <title>Let's start baking! - Gapshift</title>
          <meta name="description" content="Please provide us with more information about your client and the project. Try and be as throrough as possible with your requirements." />
        </Helmet>

        {/* Page title */}
        <Section class="container-fluid">
          <div className="container">
        		<div className="row page-title">
              <div className="col s12">
                <div className="vert-spacer h8 hide-on-med-and-down"></div>
        				<h1 className="shadow dull-black-text">Let's start baking</h1>
                <h5>Please provide us with more information about your client and the project. Try and be as throrough as possible with your requirements.</h5>
        				<Curly dark />
        			</div>
        		</div>

            <div className="row">
              <form id="project-form" className="col s12 m8" noValidate>
                <div className="col s12">
                  <p>
                    <i className="far fa-user-circle"></i> <b>Personal details</b>
                  </p>
                </div>
          			<div className="col s12 m6">
                  <label htmlFor="project_contact_name">Name*</label>
          				<input type="text" name="project_contact_name" placeholder="Name" pattern=".{2,}" required />
          			</div>
          			<div className="col s12 m6">
                  <label htmlFor="projects_contact_surname">Surname*</label>
          				<input type="text" name="project_contact_surname" placeholder="Surname" pattern=".{2,}" required />
          			</div>
          			<div className="col s12">
                  <label htmlFor="projects_contact_email">Email*</label>
          				<input type="email" name="project_contact_email" placeholder="Email" pattern="[a-zA-Z0-9._%+-]{1,}@[a-z0-9.-_]{1,}\.[a-z]{2,63}$" required />
          			</div>
          			<div className="col s12 m6">
                  <label htmlFor="projects_contact_number">Mobile number</label>
          				<input type="tel" name="project_contact_number" placeholder="e.g. 084 123 4567" pattern="[0-9]{2,3}[\s]{0,1}[0-9]{3}[\s]{0,1}[0-9]{4}$" />
          			</div>

                <div className="col s12">
                  <div className="vert-spacer h2" />
                  <p>
                    <i className="far fa-bookmark"></i> <b>Client/Company and project details</b>
                  </p>
                </div>
                <div className="col s12">
                  <label htmlFor="project_client">Client/Company name*</label>
          				<input type="text" name="project_client" placeholder="Client/Company name" pattern=".{2,}" required />
          			</div>
                <div className="col s12">
                  <label htmlFor="project_name">Project name*</label>
          				<input type="text" name="project_name" placeholder="Project name" pattern=".{2,}" required />
          			</div>
                <div className="col s12">
                  <label htmlFor="project_deadline">Deadline*</label>
          				<input type="date" name="project_deadline" pattern="\d{1,2}[\/\-]\d{1,2}[\/\-]\d{2,4}$" required />
          			</div>
                <div className="col s12">
                  <label htmlFor="project_description">Description*</label>
          				<textarea name="project_description" placeholder="Give a short overview of your project requirements" required></textarea>
          			</div>
                <div className="col s12">
                  <input type="text" id="bots" className="bots-honeypot" />
                  <div className="vert-spacer h1" />
      						<button type="submit" className="waves-effect waves-dark btn-large moon-blue-btn">Send</button>
      					</div>
                <div className="vert-spacer h5" />
              </form>
            </div>
        	</div>
        </Section>
      </div>
    );
  }
}

export default NewProjectPage;

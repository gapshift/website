var openModal;
var heroDataReceived;

jQuery.noConflict();
(function( $ ) {
	$(function() {

	/*
	=================================================
	Bind the touch event for mobile devices
	=================================================
	*/
	$('body').bind('touchstart', function() {});



	/*
	=========================================================
	Randomize hero letters + animation sequence + tilt effect
	=========================================================
	*/

	// tilt effect
	$('.tilt').tilt({
		maxTilt:        15,
		perspective:    1800,   // Transform perspective, the lower the more extreme the tilt gets.
		easing:         "cubic-bezier(.03,.98,.52,.99)",    // Easing on enter/exit.
		scale:          1.05,      // 2 = 200%, 1.5 = 150%, etc..
		speed:          800,    // Speed of the enter/exit transition.
		transition:     true,   // Set a transition on enter/exit.
		disableAxis:    null,   // What axis should be disabled. Can be X or Y.
		reset:          true,   // If the tilt effect has to be reset on exit.
		glare:          false,  // Enables glare effect
		maxGlare:       0.2       // From 0 - 1.
	});
	
	// preload the images needed
	var imgLoadCounter = 0;
	var imgPath = "https://s3-eu-west-1.amazonaws.com/gapshift-static-web-assets/hero/";
	var letterHolder = $('#hero-letter');
	var heroLoader = $('.hero-loader');
	var min = 1;
	var max = heroDataReceived.colors.length;
	var lettersArr = heroDataReceived.letters;
	var imgTot = max * lettersArr.length;
	var keyTot = 0;
	var ranN = 0;
	var prevRanN = 1;

	//hide the image holder
	letterHolder.hide();
	//load all the images into the holder
	$.each(lettersArr,function(index,value){
		for(var i = 1; i < max+1; i++){
			keyTot++;
			letterHolder.append('<img key="'+keyTot+'" src="'+imgPath+value+'/'+i+'.png" />');
		}
	});
	//check if all images have loaded and show the holder when done
	$('#hero-letter img').on("load", function(){
		imgLoadCounter++;
		if(imgLoadCounter >= imgTot){
			console.log('all images loaded');
			//hide all the images at first
			letterHolder.children().hide();
			//show the holder
			letterHolder.show();
			heroLoader.fadeOut();
			startLetterAnimation();
		}
	});


	var letterCounter = 0;
	var tl = new TimelineMax();
	var int = 300;
	var letter = $('#hero-letter img');
	var headline = $('#hero-headline');
	var windowW = $(window).width();
	var interval;

	function intervalFuncA(){
		//generate a random number
		setRandomN();

		//hide all the images again
		letterHolder.children().hide();

		//show the random image
		letterHolder.find('[key="'+ranN+'"]').show();

		//get the number of the image file
		var src = letterHolder.find('[key="'+ranN+'"]').attr('src');
		var filename = src.substring(src.lastIndexOf('/')+ 1);
		var colorNum = filename.replace(/.png/gi,"");
		//console.log(colorNum);
		
		//set the background color
		$('#hero').css({ backgroundColor: heroDataReceived.colors[colorNum - 1] });
	}

	function intervalFuncB(){
		//set the image
		tl.set($('#hero'),{transition: 'all 0.5s', overflow: 'hidden'});
		intervalFuncA();
	}

	function randomNumber(min,max){
		return Math.floor(Math.random() * (max - min + 1)) + min;
	}

	var a = 0;
	function setRandomN(){
		var set = {
			1: [1,31],
			2: [32,62],
			3: [62,92]
		};
	
		if(a >= 3){
			a = 1
		}else{
			a++;
		}

		ranN = randomNumber(set[a][0],set[a][1]);
		//console.log(ranN);
	}

	function resetInt(){
		clearInterval(interval);
		int = 5000;
		interval = setInterval(intervalFuncB, int);
	}

	function startLetterAnimation(){

		//start the letter switch
		interval = setInterval(intervalFuncA, int);

		//start the scaling in animation
		if(windowW >= 992){
			tl.set(letter,{left: '50%', transform: 'translateX(-50%)'});
		}
		//tl.fromTo(letter, 5, {scale: 0}, {scale: 1, ease:Power2.easeOut, onComplete:resetInt});
		tl.fromTo(letter, 5, {scale: 0}, {scale: 1, ease:ExpoScaleEase.config(0.1, 1, Power1.easeInOut), onComplete:resetInt});
		if(windowW >= 992){
			tl.to(letter, 1,{left: 0, ease:Power1.ease});
		}
		tl.staggerFromTo(headline.children(), 0.8, {left: '-=40%', opacity: 0}, {left: 0, opacity: 1, ease:Power1.ease}, 0.2, '-=0.8');
	}

	// animate the down arrow
	TweenMax.to('.down-arrow-start', 0.3, {bottom: '+=5', ease:Power2.easeIn, yoyo:true, repeat: -1});


	/*
	=========================================================
	Word ticker effect
	=========================================================
	*/
	var tickerArr = ["ideas","designs","illustrations","web developments","mobile apps","animations","copylines","productions","post-productions","online campaigns","tv adverts"];
	var tickerCounter = 0;
	var tickerSpeed = 5;
	var tickerArrLength = tickerArr.length - 1;
	var tickerTxt = tickerArr[tickerCounter];
	var tickerDuration = tickerTxt.length / tickerSpeed;
	var tickerTl = new TimelineMax();
	var typing = false;

	//blink the ticker cursor
	function startBlink(){
		TweenMax.fromTo($('.ticker-cursor'), 0.1, {opacity: 0}, {opacity: 1, ease:Power1.easeOut, repeat: -1, yoyo:true, repeatDelay: 0.3});
	}

	//animate the ticker
	doTicker();
	startBlink();
	function doTicker(){
		tickerTl.fromTo($('.ticker'), tickerDuration, {text:{value: ""}}, {text:{value: tickerTxt}, ease:Linear.easeNone, yoyo: true, repeat: 1, repeatDelay: 3, onComplete: doTicker});
		changeProdText();
	}

	function changeProdText(){
		if(tickerCounter < tickerArrLength){
			tickerCounter += 1;
		}else if(tickerCounter >= tickerArrLength){
			tickerCounter = 0;
		}
		tickerTxt = tickerArr[tickerCounter];
		//console.log(tickerTxt.toString());
	}


	/*
	=================================================
	Materialize events
	=================================================
	*/
	$('.button-collapse').sideNav({
      closeOnClick: true
    }
  );

	$('.scrollspy').scrollSpy({
		scrollOffset: 60
	});

	//show loader
	function showLoader(){
		$('.loader').show();
	}

	//hide loader
	function hideLoader(){
		$('.loader').hide();
	}

	$('a').click(function(e){
		if(/#/.test(this.href)){
			var urlpath = window.location.pathname;
			var newpath = $(this).attr('href');
			var hrefWithoutHash = newpath.split('#');
			console.log(hrefWithoutHash[0]);

			if(urlpath !== hrefWithoutHash){
				window.location = '/'+newpath;
			}
		}

	})

	/*
	=================================================
	Projects Owl Carousel
	=================================================
	*/
	var carousel_settings = {
		items: 1,
		margin: 0,
		dots: true,
		dotsContainer: $('.pagination-dots'),
		lazyLoad: true,
	};
	function displaySlider(){
		hideLoader();
		$('#slider-holder').show();
		//reinitialize the carousel
		$('#slider').owlCarousel(carousel_settings);
	}


	/*
	=================================================
	Clients Carousel
	=================================================
	*/
	$("#clients .owl-carousel").owlCarousel({
		stagePadding: 0,
		dots: false,
		//autoWidth: true,
		//margin: 100,
		autoplay: true,
		autoplayTimeout: 3000,
		autoplaySpeed: 800,
		center: true,
		loop: true,
		responsive: {
			0: {
				items: 1
			},
			420: {
				items: 2
			},
			768: {
				items: 3
			},
			960: {
				items: 4
			},
			1280: {
				items: 6
			}
		}
	});



	/*
	=================================================
	Modal functionality
	=================================================
	*/
	$('body').on('keydown',function(e) {
		var owl = $('#pmodal .owl-carousel');
		switch(e.which) {
			case 27: // esc
			$('.modal').modal('close');
			break;

      case 37: // left
			owl.trigger('prev.owl.carousel');
      break;

      case 39: // right
			owl.trigger('next.owl.carousel');
      break;

      default: return; // exit this handler for other keys
    }
	});
	$('.modal').modal({
		startingTop: '40%',
		endingTop: '50%',
		complete: function(){
			$("#vmodal iframe").attr('src','');
		},
		ready: function(){
			$('ul.tabs').tabs();
			$('[data-toggle="datepicker"]').datepicker({
				zIndex: 9999,
				format: 'dd-mm-yyyy'
			});
		}
	});
	$('.modal .closebtn').click(function(){
		//console.log('close modal');
		$('.modal').modal('close');
	})

	// open modal function
	openModal = function(params){
		//console.log(params);

		var vidID = params.vidID;
		var modal = params.modal;

		if(vidID){
			//show vmodal
			var vidList = params.vidList;
			var vidUrlPre = "https://www.youtube.com/embed/";
			var vidUrlPost = "?rel=0&list="+vidList;
			$('#'+modal+' iframe').attr('src', vidUrlPre+vidID+vidUrlPost);
			$('#'+modal).modal('open');
		}else{
			//show pmodal
			var dir = params.slug+'/';
			var fileCount = params.fileCount;
			var content = "";
			var projDir = params.projDir;

			// show the loader and hide the slider when loading
			showLoader();
			$('#slider-holder').hide();

			//remove the owl if exists and/or create a new one
			if($('#slider').length > 0){
				$('#slider').remove();
				$('#slider-holder').prepend('<div id="slider" class="owl-carousel"></div>');
			}else{
				$('#slider-holder').prepend('<div id="slider" class="owl-carousel"></div>');
			}

			// loop through the received fileCount and add an <img> for each
			for(var i=1; i<fileCount+1; i++){
				if(i<10){
					i = '0'+i;
				}
				content += '<img src="'+projDir+dir+i+'.jpg" alt="" />'
			};

			// add the content to the slider
			$('#'+modal+' #slider').html(content);

			// display the slider after all images have loaded
			var a = 0;
			$('#'+modal+' #slider img').on("load", function() {
				a++
				if(a === fileCount){
					displaySlider();
				}
			});

			// show the modal
			$('#'+modal).modal('open');
		}
	}


	// form validation on blur
	function validate(elem,elemVal,isRequired){
		//elem = $(this);

		if(isRequired){
			//console.log('yes');
			if(elemVal === ''){
				elem.addClass('invalid');
			}else if(elemVal !== '' && !elem.is(':valid')){
				elem.addClass('invalid');
			}else{
				elem.removeClass('invalid');
			}
		}else{
			//console.log(elemVal);
			if(elemVal !== '' && !elem.is(':valid')){
				elem.addClass('invalid');
			} else {
				elem.removeClass('invalid');
			}
		}
	}

	$('input, textarea, select').each(function(){
		var elem = $(this);
		var isRequired = elem.attr('required');

		elem.blur(function(){
			var elemVal = elem.val();
			validate(elem,elemVal,isRequired);
		})
	});

	// form submition function
	$('form').on('submit',function(e){
		e.preventDefault();
		var formID = $(this).attr('id');

		var params = {
			//,
			form: $('#'+formID),
			formData: {},
			webhooks: {
				hookUrl: "",
				//contactHook: "https://hooks.zapier.com/hooks/catch/2778915/schpc0/",
				contactHook: "https://nh76wbl1c4.execute-api.eu-west-1.amazonaws.com/prod/contact",
				//projectHook: "https://hooks.zapier.com/hooks/catch/2778915/scvamd/"
				projectHook: "https://nh76wbl1c4.execute-api.eu-west-1.amazonaws.com/prod/newproject",
				legendHook: "https://nh76wbl1c4.execute-api.eu-west-1.amazonaws.com/prod/creative-legends"
			},
			isValid: true
		}

		//form validation
		$('input, textarea, select').each(function(){
			var elem = $(this);
			var elemVal = elem.val();
			var isRequired = elem.attr('required');

			validate(elem,elemVal,isRequired);
		});

		var hasInvalid = params.form.find('.invalid').length;
		if(hasInvalid){
			params.isValid = false;
		}

		// check bots
		if($("#bots").val() !== ""){
			params.isValid = false;
		}


		//if all passed
		if(params.isValid) {

			// show the loader
			params.form.find('button[type="submit"]').attr('disabled', true);
			params.form.find('button[type="submit"]').html('•••').addClass('pulse sending');

			console.log('starting '+params.form+' submission...');

			function objectifyForm(formArray) {//serialize data function
			  var returnArray = {};
			  for (var i = 0; i < formArray.length; i++){
			    returnArray[formArray[i]['name']] = formArray[i]['value'];
			  }
			  return returnArray;
			}

			params.formData = objectifyForm(params.form.serializeArray());
			//console.log('sent data:');
			//console.log(JSON.stringify(params.formData));

			// set the webhook url
			if(formID === "contact-form"){
				params.webhooks.hookUrl = params.webhooks.contactHook;
			}else if(formID === "project-form"){
				params.webhooks.hookUrl = params.webhooks.projectHook;
			}else if(formID === "creative-legends-form"){
				params.webhooks.hookUrl = params.webhooks.legendHook;
			}

			$.ajax({
				type: "POST",
				url: params.webhooks.hookUrl,
				crossDomain: true,
				contentType: "application/json",
				data: JSON.stringify(params.formData),
				success: function(response){

					// show the response
					//console.log('returned data:');
					//console.log(response);

					// reset the form
					params.form[0].reset();
					params.form.find('label').removeClass('active');
					params.form.find('label[data-type="tel"]').addClass('active');
					params.form.find('button[type="submit"]').removeAttr('disabled');
					params.form.find('button[type="submit"]').html('Send').removeClass('pulse sending');

					console.log(params.form+' submition complete\n\n');

					// Materialize.toast(message, displayLength, className, completeCallback);
					if(response === 0){
						Materialize.toast('Message sent, thank you!', 4000);
					}else if(response === 1){
						Materialize.toast('New project request sent, thank you!', 4000);
					}else if(response === 2){
						Materialize.toast('Legend application sent, thank you!', 4000);
					}

				},
				error: function(response){
					console.log(response.responseText);
					Materialize.toast('Your request could not be sent. Please try again later.', 4000)
				}
			});
		}
	});

	// init the select
	$('select').material_select();


	/*
	=================================================
	Parallax
	=================================================
	*/

	// init controller
	var controller = new ScrollMagic.Controller();

	// timeline
	if($('.timeline').length > 0){
		var tween2 = TweenMax.staggerFromTo('.timeline li', 0.15, {y: "+=50px", opacity: 0}, {y: "-=50px", opacity: 1, ease: Power1.easeInOut}, 0.2);
		new ScrollMagic.Scene({triggerElement: '#how-we-work', duration: $('.timeline').height()})
		.setTween(tween2)
		//.addIndicators()
		.addTo(controller);
	}

	// feature box
	if($('.feature-boxes').length > 0){
		var tween3 = TweenMax.staggerFromTo('.feature-boxes li', 0.4, {opacity: 0}, {opacity: 1, ease: Power1.easeInOut}, 0.1);
		new ScrollMagic.Scene({triggerElement: '#services', duration: $('.feature-boxes').height()})
		.setTween(tween3)
		//.addIndicators()
		.addTo(controller);
	}

	// page title
	if($('.page-title').length > 0){
		var tween4 = TweenMax.staggerTo('.page-title h1, .page-title h5, .page-title span', 0.5, {y: "-=50px", opacity: 0, ease: Power1.easeInOut}, 0.1);
		new ScrollMagic.Scene({triggerElement: '.page-title', duration: $('.page-title').height(), triggerHook: 'onLeave', offset: -140})
		.setTween(tween4)
		//.addIndicators()
		.addTo(controller);
	}


	/*
	=================================================
	Scroll to anchor function
	=================================================
	*/
	function scrollTo(tar){
		TweenMax.to(window, 0.8, {scrollTo:tar, ease: Power4.easeInOut});
	}


	/*
	=================================================
	Link & button click function for anchors
	=================================================
	*/
	$('a[href*="#"]').click(function(e){

		// check if it is a tab
		var isTab = $(this).is('[tab]');
		//console.log(isTab);

		if(isTab){
			e.preventDefault();
			//var href = $(this).attr('href');
			//scrollTo(href);
		}
	});


	/*
	=================================================
	Work tabs filter functionality
	=================================================
	*/

	$('#work .tabs li a').click(function(e){
		e.preventDefault();
		var target = $(this).data('target');
		//console.log(target);

		if(target){
			if(target === "all"){
				$('#projects [data-category]').show();
			}else{
				$('#projects [data-category]').hide();
				$('#projects [data-category="'+target+'"]').show();
			}
		}
	});


	/*
	=================================================
	Apply class when navbar reaches the top
	=================================================
	*/
	var $window = $(window);
	var $menu = $('#NavBar nav');
	var menuPos = $menu.offset().top;

	$window.on("scroll", function(){
		var scrollPos = $window.scrollTop();
		

		if (scrollPos > 0) {
			$menu.addClass('onScroll');
			//$('.navbarstyle').addClass('sticky-scale');
			//$('#NavBar').css({position: 'fixed', marginTop: 0});
		}else{
			$menu.removeClass('onScroll');
			//$('.navbarstyle').removeClass('sticky-scale');
			//$('#NavBar').css({position: 'relative', marginTop: 70});
		}
	});

	});
})(jQuery)

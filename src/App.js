import React, { Component } from 'react';
import Routes from './routes/Routes';
import NavBar from './components/NavBar';
import Footer from './components/Footer';

class App extends Component {
  render() {
    return (
      <div>
        <NavBar />
        <Routes />
        <Footer />
      </div>

    );
  }
}

export default App;

const assetsPath = "https://s3-eu-west-1.amazonaws.com/gapshift-static-web-assets/";
const localImageAssetsPath = "./assets/images/";

export default {
    localImageAssetsPath: localImageAssetsPath,
    assetsPath: assetsPath,
    logo: assetsPath+'gapshift-logo.svg',
    digitalBakeryLogo: assetsPath+'hero/digital-bakery-dark@2x.png',
    processImage: assetsPath+'process-full.png',
    curlyDark: assetsPath+'curly-dark.png',
    curlyLight: assetsPath+'curly-light.png',
    curlyBlack: assetsPath+'curly-black.png',
    cookieCrumbsImg: assetsPath+'cookie-crumbs.svg',
    legendsHeader: assetsPath+'legends-header.png',
    footerGSIcon: assetsPath+'icons/favicon-transcut.svg',
    twitter: {
        cards: {
            default: assetsPath+'twitter/twitter-card.png',
            creativeLegends: assetsPath+'twitter/creative-legends-card.png',
        }
    },
    timeline: {
        orderIcon: assetsPath+'timeline/order-icon.svg',
        recipeIcon: assetsPath+'timeline/recipe-icon.svg',
        ovenIcon: assetsPath+'timeline/oven-icon.svg',
        magnifyIcon: assetsPath+'timeline/magnify-icon.svg',
        cupcakeBlankIcon: assetsPath+'timeline/cupcake-blank-icon.svg',
        ovenPlusIcon: assetsPath+'timeline/oven-plus-icon.svg',
        cupcakeSprinklesIcon: assetsPath+'timeline/cupcake-sprinkles-icon.svg',
    },
    freelanceBenefits: {
        checklist: assetsPath+'freelance-benefits/check-list.svg',
        clientNetwork: assetsPath+'freelance-benefits/client-network.svg',
        laptop: assetsPath+'freelance-benefits/laptop.svg',
        rate: assetsPath+'freelance-benefits/rate.svg',
        rates: assetsPath+'freelance-benefits/rates.svg',
        timeFlex: assetsPath+'freelance-benefits/time-flex.svg',
    },
    partners: {
        postCityLogo: assetsPath+'partners/post-city-logo.png',
    },
    clients: {
        absa: assetsPath+'clients/absa.png',
        bcx: assetsPath+'clients/bcx.png',
        mtn: assetsPath+'clients/mtn.png',
        ogilvy: assetsPath+'clients/ogilvy.png',
        pfs: assetsPath+'clients/pfs.png',
        pindrop: assetsPath+'clients/pindrop.png',
        standardBank: assetsPath+'clients/standard-bank.png',
        tbwa: assetsPath+'clients/tbwa.png',
        vodacom: assetsPath+'clients/vodacom.png',
    }
}
import React, { Component } from 'react';
import * as firebase from 'firebase';
import { Project } from './';
import 'firebase/firestore';

var config = {
  apiKey: "AIzaSyDpPdhf_NEXIGEatR_aZm99afYtJ82Sl98",
  authDomain: "gapshift-80997.firebaseapp.com",
  databaseURL: "https://gapshift-80997.firebaseio.com",
  projectId: "gapshift-80997",
  storageBucket: "gapshift-80997.appspot.com",
  messagingSenderId: "978335965361"
}
firebase.initializeApp(config);
var db = firebase.firestore();

class Projects extends Component {
	constructor(props){
		super(props);
		this.projRef = db.collection('projects').orderBy('dateAdded', 'desc');
		this.servRef = db.collection('services');

		this.state =  {
			projects: [],
			services: [],
			loading: true
		};
	}

	componentWillMount(){
		//this.servRef.get().then(this.getServices);
		this.servRef.onSnapshot(this.getServices);
		this.projRef.onSnapshot(this.onCollectionUpdate);
	}

	getServices = (snapshot) => {
		// const services = [];
		// snapshot.forEach(doc => {
		// 	// const { app, ba, design, motion, pm, social, web, write } = doc.data();
		// 	// services.push(app, ba, design, motion, pm, social, web, write);
		// 	services.push(doc.data());
		// });
		this.setState({
		  services: snapshot
		});
		//console.log(this.state.services);
	}

	onCollectionUpdate = (snapshot) => {
	  this.setState({
	    projects: snapshot,
			loading: false,
	 	});
		//console.log(this.state.projects);
	}


  render() {
		const { projects, services } = this.state;

		if (this.state.loading) {
    	return ( // or render a loading icon
				<div className="col s2 offset-s5 progress" style={{marginTop: 40}} >
					<div className="indeterminate chirpy-purple-bg" />
				</div>
			);
  	}
		return(
			<div className="col s12 left-align" id="projects" >
				{
					//console.log(services.docs[0].data())
				}
				{
					projects.docs.map((doc,index) => (
						<Project key={index} id={doc.id} item={doc.data()} catName={services.docs[0].data()[doc.data().category]}  modal={doc.data().vidID ?  'vmodal' : 'pmodal'} />
					))
				}
			</div>
		);
  }
};

export default Projects;

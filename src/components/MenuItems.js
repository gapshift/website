import React, { Component } from 'react';
class MenuItems extends Component {
  render() {
    return (
			<div>
				<li><a href="#how-we-work">How we work</a></li>
				<li><a href="#services">Services</a></li>
				<li><a href="#work">Work</a></li>
				<li><a href="/contact" >Contact us</a></li>
				<li><a href="/newproject" className="waves-effect waves-dark btn aqua-green-btn" menu-btn="true">Let's Bake</a></li>
			</div>
    );
  }
}

export default MenuItems;

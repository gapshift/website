import React, { Component } from 'react';
import { MenuItems } from './';
import Assets from '../constants/Assets';

class NavBar extends Component {
  render() {
    return (
      <div id="NavBar">
				<nav className="navbarstyle">
					<div className="container">
            <div className="col s12">
              <a href="/" className="brand-logo"><img src={Assets.logo} alt="Gapshift" className="logo" /></a>
              <a data-activates="mobile-demo" className="button-collapse"><i className="material-icons">menu</i></a>
              <ul className="right hide-on-med-and-down">
                <MenuItems />
              </ul>
              <ul className="side-nav" id="mobile-demo">
                <MenuItems />
              </ul>
            </div>
					</div>
				</nav>
      </div>
    );
  }
}

export default NavBar;

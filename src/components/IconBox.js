import React from 'react';

function IconBox(props) {
  return(
    <li className="col s12 m6 center-align icon-box tilt">
      <div className="col s12">
        <img src={`${props.icon}`} alt="" />
      </div>
      <div className="vert-spacer h2 hide-on-med-and-up"></div>
      <div className="col s12">
        <h4>{props.title}</h4>
        {props.children}
      </div>
    </li>
  );
}

export default IconBox;

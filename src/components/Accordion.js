import React, { Component } from 'react';
import { AccordionItem } from './';

class Accordion extends Component {
  render() {
    return (
      <ul className="collapsible" data-collapsible="accordion">
        <AccordionItem  header="What personal information do we collect from the people that visit our blog, website or app?">
          <p>When ordering or registering on our site, as appropriate, you may be asked to enter your name, email address, phone number, or other details to help you with your experience.</p>
        </AccordionItem>
        <AccordionItem header="When do we collect information?">
          <p>We collect information from you when you place an order, subscribe to a newsletter, respond to a survey, fill out a form, enter information on our site or provide us with feedback on our products or services.</p>
        </AccordionItem>
        <AccordionItem header="How do we use your information?">
          <p>We may use the information we collect from you when you register, make a purchase, sign up for our newsletter, respond to a survey or marketing communication, surf the website, or use certain other site features in the following ways:</p>
          <ul className="browser-default">
            <li>To personalize your experience and to allow us to deliver the type of content and product offerings in which you are most interested.</li>
            <li>To improve our website in order to better serve you.</li>
            <li>o allow us to better service you in responding to your customer service requests.</li>
            <li>To administer a contest, promotion, survey or other site feature.</li>
            <li>To ask for ratings and reviews of services or products.</li>
            <li>To follow up with them after correspondence (live chat, email or phone inquiries)</li>
          </ul>
        </AccordionItem>
        <AccordionItem header="How do we protect your information?">
          <p>We do not use vulnerability scanning and/or scanning to PCI standards.<br />
            We only provide articles and information.<br />
            We never ask for credit card numbers.<br />
          We use regular Malware Scanning.</p>
          <p>Your personal information is contained behind secured networks and is only accessible by a limited number of persons who have special access rights to such systems, and are required to keep the information confidential. In addition, all sensitive/credit information you supply is encrypted via Secure Socket Layer (SSL) technology.</p>
          <p>We implement a variety of security measures when a user places an order enters, submits, or accesses their information to maintain the safety of your personal information.</p>
          <p>All transactions are processed through a gateway provider and are not stored or processed on our servers.</p>
        </AccordionItem>
        <AccordionItem header="Do we use 'cookies'?">
          <p>We do not use cookies for tracking purposes</p>
          <p>You can choose to have your computer warn you each time a cookie is being sent, or you can choose to turn off all cookies. You do this through your browser settings. Since browser is a little different, look at your browser's Help Menu to learn the correct way to modify your cookies.</p>
          <p>If you turn cookies off, some features may be disabled that make your site experience more efficient and may not function properly.</p>
        </AccordionItem>
        <AccordionItem header="Third-party disclosure">
          <p>We do not sell, trade, or otherwise transfer to outside parties your Personally Identifiable Information unless we provide users with advance notice. This does not include website hosting partners and other parties who assist us in operating our website, conducting our business, or serving our users, so long as those parties agree to keep this information confidential. We may also release information when it's release is appropriate to comply with the law, enforce our site policies, or protect ours or others' rights, property or safety.</p>
          <p>However, non-personally identifiable visitor information may be provided to other parties for marketing, advertising, or other uses.</p>
        </AccordionItem>
        <AccordionItem header="Third-party links">
          <p>We do not include or offer third-party products or services on our website.</p>
        </AccordionItem>
        <AccordionItem header="Google">
          <p>Google's advertising requirements can be summed up by <a href="https://support.google.com/adwordspolicy/answer/1316548?hl=en" target="_blank" rel="noopener noreferrer">Google's Advertising Principles.</a> They are put in place to provide a positive experience for users.</p>

          <p>We use Google Analytics on our website and have implemented the following:</p>
          <ul className="browser-default"><li>Demographics and Interests Reporting</li></ul>

          <p>We, along with third-party vendors such as Google use first-party cookies (such as the Google Analytics cookies) and third-party cookies (such as the DoubleClick cookie) or other third-party identifiers together to compile data regarding user interactions with ad impressions and other ad service functions as they relate to our website.</p>

          <p><b>Opting out:</b><br/>
          Users can set preferences for how Google advertises to you using the Google Ad Settings page. Alternatively, you can opt out by visiting the Network Advertising Initiative Opt Out page or by using the Google Analytics Opt Out Browser add on.</p>
        </AccordionItem>
        <AccordionItem header="How does our site handle Do Not Track signals?">
          <p>We honor Do Not Track signals and Do Not Track, plant cookies, or use advertising when a Do Not Track (DNT) browser mechanism is in place.</p>
        </AccordionItem>
        <AccordionItem header="Does our site allow third-party behavioral tracking?">
          <p>It's also important to note that we do not allow third-party behavioral tracking</p>
        </AccordionItem>
        <AccordionItem header="Nondisclosure agreement (NDA) for Gapshift clients">
          <p>Gapshift respects your professional artwork as well as other intellectual property shared with us and will only use it to assist you with your goals. Your copyrights will remain yours.</p>
          <p>Gapshift will not take your artwork and other intellectual property for use on any other site, resell it, or give it to anyone under any circumstance.</p>
          <p>guarantees that all information that you share with us, including artwork and other intellectual property, contact information, and client notes, will remain confidential.</p>
          <p>Notwithstanding the foregoing, P2H may disclose any such information in response to a valid order of a court or other government body or as required by law, but only that portion of the Confidential Information which is legally required to be disclosed.</p>
        </AccordionItem>
        <AccordionItem header="Contacting Us">
          <p>Should you have any further questions regarding this privacy policy, or any questions or concerns about <a href="/#services">Gapshift's services</a>, please send us a message via our <a href="/contact">contact form</a> or email us at <a href="mailto:more@gapshift.com">more@gapshift.com</a></p>
        </AccordionItem>
      </ul>
    );
  }
}

export default Accordion;

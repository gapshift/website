import React from 'react';

function ClientCarItem(props) {
  return (
    <div>
      <img src={props.image} alt={props.alt} />
    </div>
  );
}

export default ClientCarItem;

import React, { Component } from 'react';
import Assets from '../constants/Assets';

class Curly extends Component {

  render() {
		const { dark, black } = this.props;

		if(dark){
            return(
                <span className="curly">
                    <img src={Assets.curlyDark} alt="" />
                </span>
            );
        }else if(black){
            return(
                <span className="curly">
                    <img src={Assets.curlyBlack} alt="" />
                </span>
            );
        }else{
            return(
                <span className="curly">
                    <img src={Assets.curlyLight} alt="" />
                </span>
            );
        }
    }
}

export default Curly;

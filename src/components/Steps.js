import React, { Component } from 'react';
import { StepCard } from './';
import Assets from '../constants/Assets';

class Steps extends Component {
  render() {
    return (
      <ol>
        <StepCard icon={Assets.timeline.orderIcon} title="Place your order" left="true">
          <p>The process starts when you submit your brief. We will also need all relevant open files (PSD, Sketch, Adobe XD, PNG, Ai, JPG, Indd, etc.) and your thorough requirements.</p>
        </StepCard>
        <StepCard icon={Assets.timeline.recipeIcon} title="Define the recipe">
          <p>Once we’ve received your brief, a project manager will  get back to you to discuss everything essential about your project and clear up any uncertainties.</p>
        </StepCard>
        <StepCard icon={Assets.timeline.ovenIcon} title="Baking starts" left="true">
          <p>We get the mixers ready and preheat the ovens with a project manager allocating the right skill and team to the project and will keep a close eye on this masterpiece throughout the whole process.</p>
        </StepCard>
        <StepCard icon={Assets.timeline.magnifyIcon} title="QA & testing">
          <p>We always double check to make sure that we only use the best and freshest ingredients, ensuring we deliver a project that is baked to perfection and of the highest quality.</p>
        </StepCard>
        <StepCard icon={Assets.timeline.cupcakeBlankIcon} title="Doneness check" left="true">
          <p>You will receive a preview of your project for review, making sure that we’ve ticked all the boxes. We’ll then wait for your approval or any further instruction should any additional elements or changes be required.</p>
        </StepCard>
        <StepCard icon={Assets.timeline.ovenPlusIcon} title="Final baking">
          <p>After you have completed your review, we’ll add any final finishing touches that you may have requested, making sure the baking reaches the perfect temperature.</p>
        </StepCard>
        <StepCard icon={Assets.timeline.cupcakeSprinklesIcon} title="Baking complete" left="true">
          <p>Now that timer has gone and your project is complete, we’ll provide you with a payment instruction. After payment completion, you’ll receive your project with extra spinkles on top.</p>
        </StepCard>
      </ol>
    );
  }
}

export default Steps;

import React, { Component } from 'react';
import { Section } from './';
import Assets from '../constants/Assets';

//var letterCounter = -1;
var heroData = require('../data/heroSectionData.json');
var min = 0;
var max = heroData.colors.length;
var int = 2000;

class HeroSection extends Component {
  constructor(props){
    super(props);
    this.state = {
      letterCounter: null,
      ranN: null,
      prevRanN: null
    }
  }

  getRandomNum(min,max){
    return Math.floor(Math.random() * (max - min + 1) ) + min;
  }

  setRandomNum(){
    if(this.state.ranN === this.state.prevRanN){
      this.setState({
        ranN: this.state.ranN + 1,
        prevRanN: this.state.ranN
      })
    }else{
      this.setState({
        ranN: this.getRandomNum(min,max),
        prevRanN: this.state.ranN
      })
    }
  }

  componentWillMount(){
    this.setState({
      letterCounter: 0,
      ranN: this.getRandomNum(min,max)
    })
  }

  componentDidMount(){
    this.heroInterval = setInterval(() => {
      this.setRandomNum();
      if(this.state.letterCounter >= 0 && this.state.letterCounter < 2){
        this.setState({
          letterCounter: this.state.letterCounter + 1
        })
      }else{
        this.setState({
          letterCounter: 0
        })
      }
    }, int)
  }

  componentWillUnmount(){
    clearInterval(this.heroInterval);
  }

  render() {
    const { letterCounter, ranN, prevRanN } = this.state;
    return (
      <Section
        id="hero" 
        className="container-fluid valign-wrapper"
        style={{ backgroundColor: heroData.colors[ranN - 1] }}
      >
        <div className="container">
          <div className="row valign-wrapper">
            <div id="hero-letter" className="col s12 l6 center-align">
              <img src={`./assets/images/hero/${heroData.letters[letterCounter]}/${ranN}.png`} alt="" />
            </div>
            <div className="vert-spacer h4 hide-on-large-only"></div>
            <div className="col l1 show-on-large"></div>
            <div id="hero-headline" className="col s12 l5">
              <h2><b>AN UNUSUAL,<br/>NO BOUNDRIES</b></h2>
              <img id="digitalBakeryLogo" src={Assets.digitalBakeryLogo} alt="" />
            </div>
          </div>
        </div>
      </Section>
    );
  }
}

export default HeroSection;

import React, { Component } from 'react';
import { Modal } from './';

class Steps extends Component {
  render() {
    return (
      <div>
        <Modal id="pmodal">
          <div id="slider-holder" className="container-fluid">
      			<div className="pagination-dots"></div>
      		</div>
        </Modal>
        <Modal id="vmodal">
          <div id="slider-holder" className="container-fluid video-container">
      			<iframe title="Youtube clips" width="560" height="315" src="" frameBorder="0" allowFullScreen></iframe>
      		</div>
        </Modal>
      </div>
    );
  }
}

export default Steps;

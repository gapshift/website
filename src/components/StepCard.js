import React from 'react';

function StepCard(props) {
  if(props.left){
    return(
      <li>
        <div className="col s12 l5 push-l7 icon">
          <img src={props.icon} alt={props.title} />
        </div>
        <div className="col s12 l7 pull-l5">
          <h3>{props.title}</h3>
          {props.children}
        </div>
      </li>
    );
  }else{
    return(
      <li>
        <div className="col s12 l5 icon">
          <img src={props.icon} alt={props.title} />
        </div>
        <div className="col s12 l7">
          <h3>{props.title}</h3>
          {props.children}
        </div>
      </li>
    );
  }
}

export default StepCard;

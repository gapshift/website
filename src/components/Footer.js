import React, { Component } from 'react';
import Assets from '../constants/Assets';

class Footer extends Component {
  render() {
    const { psLogo } = partnerLogos;
    return (
      <div id="Footer">
  			<footer className="page-footer">
  				<div className="container">
  					<div className="row">
  						<div className="col s12 m3">
  							<h6 className="white-text">Learn more</h6>
  							<ul>
  								<li><a className="grey-text text-lighten-3" href="#how-we-work">How we work</a></li>
  								<li><a className="grey-text text-lighten-3" href="#services">Services</a></li>
  								<li><a className="grey-text text-lighten-3" href="/creative-legends">Creative legends</a></li>
  								<li><a className="grey-text text-lighten-3" href="/privacy">Privacy policy</a></li>
  							</ul>
  						</div>
  						<div className="col s12 m3">
  							<h6 className="white-text">Speak to us</h6>
  							<ul>
  								<li><a href="/newproject" className="grey-text text-lighten-3">Let's bake</a></li>
  								<li><a href="/contact" className="grey-text text-lighten-3">Contact us</a></li>
  							</ul>
  						</div>
  						<div className="col s12 m3">
  							<h6 className="white-text">Stay connected</h6>
  							<ul>
  								<li>
  									<a className="grey-text text-lighten-3" href="https://www.facebook.com/gapshift/" target="_blank" rel="noopener noreferrer"><i className="icon ion-social-facebook"></i></a>
  									<a className="grey-text text-lighten-3" href="https://youtube.com/channel/UCXePwOatGtpvAH4GRwd4eTg" target="_blank" rel="noopener noreferrer"><i className="icon ion-social-youtube"></i></a>
  									<a className="grey-text text-lighten-3" href="https://twitter.com/gapshift" target="_blank" rel="noopener noreferrer"><i className="icon ion-social-twitter"></i></a>
  									<a className="grey-text text-lighten-3" href="https://www.linkedin.com/company/gapshift/" target="_blank" rel="noopener noreferrer"><i className="icon ion-social-linkedin"></i></a>
  								</li>
  							</ul>
  						</div>
              <div className="col s12 m3">
                <a href="http://www.postcity.tv" target="_blank" rel="noopener noreferrer"><img src={Assets.partners.postCityLogo} style={psLogo} alt="Proud member of the Post City Network" /></a>
              </div>
  					</div>
  				</div>
  				<div className="footer-copyright">
  					<div className="container valign-wrapper">
              <img className="gs-icon" src={Assets.footerGSIcon} alt="Gapshift" /> © Gapshift (Pty) Ltd.
  					</div>
  				</div>
  			</footer>
      </div>
    );
  }
}

var partnerLogos = {
  psLogo: {
    width: 100,
    height: 'auto',
    padding: 0,
    margin: 0,
    marginBottom: 25,
    display: 'block',
    fontSize: 0
  },
}

export default Footer;

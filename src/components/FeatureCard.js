import React from 'react';

function FeatureCard(props) {
  return(
    <li className="col s12 m6 left-align feature-box">
      <div className="col s12 m4 l3 xl3">
        <div className={`icon-circle icon-xl align-top icon-${props.icon}`}><i className="material-icons"></i></div>
      </div>
      <div className="vert-spacer h2 hide-on-med-and-up"></div>
      <div className="col s12 m8 l9 xl9">
        <h5>{props.title}</h5>
        {props.children}
      </div>
    </li>
  );
}

export default FeatureCard;

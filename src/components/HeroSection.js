import React, { Component } from 'react';
import { Pulse } from 'react-preloading-component';
import { Section } from './';
import Assets from '../constants/Assets';

var heroData = require('../data/heroSectionData.json');
window.heroDataReceived = heroData;
var styles = {
  tiltHolder:{
    position: 'absolute', 
    width: '100%', 
    height: '100%',
    top: 0,
    left: 0,
    overflow: 'hidden'
  }
}

class HeroSection extends Component {
  render() {
    const { tiltHolder } = styles;
    return (
      <Section id="hero" className="container-fluid valign-wrapper">
        <div style={tiltHolder} className="valign-wrapper tilt">
          <div className="container">
            <div className="row valign-wrapper">
              <div id="hero-letter" className="col s12 l6 center-align" />
              <div className="vert-spacer h4 hide-on-large-only"></div>
              <div className="col l1 show-on-large"></div>
              <div id="hero-headline" className="col s12 l5">
                <h2>An unusual,<br/>no boundaries</h2>
                <img id="digitalBakeryLogo" src={Assets.digitalBakeryLogo} alt="" />
              </div>
            </div>
          </div>
          <div className="down-arrow-start">
            <a href="#intro" style={{ color: 'white' }}><i className="material-icons">arrow_downward</i></a>
          </div>
          <div className="hero-loader">
            <Pulse
              color="#57C1E8" // Default hex color (string)
              size={10} // Default Size in px (number)
            />
          </div>
        </div>
      </Section>
    );
  }
}

export default HeroSection;

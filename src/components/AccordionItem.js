import React from 'react';

function AccordionItem(props) {
  return(
    <li>
      <div className="collapsible-header">{props.header}</div>
      <div className="collapsible-body">{props.children}</div>
    </li>
  );
}

export default AccordionItem;

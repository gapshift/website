import React, { Component } from 'react';

class Project extends Component {
	constructor(props){
		super(props);
		this.handleClick = this.handleClick.bind(this);
		this.state = {
			projDir: 'https://s3-eu-west-1.amazonaws.com/gapshift-static-web-assets/projects/',
		}
	}

	handleClick(){
		let params = {
			vidID: this.props.item.vidID,
			vidList: this.props.item.vidList,
			slug: this.props.id,
			modal: this.props.modal,
			projDir: this.state.projDir,
			fileCount: this.props.item.fileCount
		}
		//console.log(params);
		window.openModal(params);
	}

  render() {
		const { category, client, description, title, services } = this.props.item;
		const { projDir } = this.state;
		const listServices = services.map((key,value) => (
			<div key={key} className={`icon-circle icon-s align-top icon-${key}`}><i className="material-icons"></i></div>
		));

		return(
			<div className="col s12 m6 l4 xl3 project-box" data-category={category} onClick={this.handleClick}>
				<div className="background" style={{ backgroundImage: `url(${projDir}${this.props.id}/thumb.png)` }} />
				<div className="text valign-wrapper">
					<div>
						<p className="s-text">{this.props.catName}</p>
						<h5>{client} {title}</h5>
						<p>{description}</p>
						{listServices}
					</div>
				</div>
			</div>
		);
  }
}

export default Project;

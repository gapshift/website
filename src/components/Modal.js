import React from 'react';
import { Loader } from './';

function Modal(props) {
  return (
    <div id={props.id} className="modal">
    	<div className="modal-content">
    		<Loader />
    		{props.children}
    	</div>
    </div>
  );
}

export default Modal;

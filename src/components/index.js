import Accordion from './Accordion';
import AccordionItem from './AccordionItem';
import ClientCarItem from './ClientCarItem';
import Curly from './Curly';
import FeatureCard from './FeatureCard';
import Footer from './Footer';
import Loader from './Loader';
import MenuItems from './MenuItems';
import Modal from './Modal';
import Modals from './Modals';
import NavBar from './NavBar';
import Project from './Project';
import Projects from './Projects';
import Section from './Section';
import Services from './Services';
import StepCard from './StepCard';
import Steps from './Steps';
import IconBox from './IconBox';
import LegendBenefits from './LegendBenefits';
import HeroSection from './HeroSection';

export {
    Accordion,
    AccordionItem,
    ClientCarItem,
    Curly,
    FeatureCard,
    Footer,
    Loader,
    MenuItems,
    Modal,
    Modals,
    NavBar,
    Project,
    Projects,
    Section,
    Services,
    StepCard,
    Steps,
    IconBox,
    LegendBenefits,
    HeroSection
 };
import React, { Component } from 'react';
import { FeatureCard } from './';

class Services extends Component {
  render() {
    return (
      <ul className="feature-boxes">
        <FeatureCard icon="web" title="Web development">
          <p>Custom website, WordPress, e-Commerce, Rich HTML5 ads (Sizmek & GDN) &amp; Mailer development.</p>
        </FeatureCard>
        <FeatureCard icon="app" title="App development">
          <p>A full-cycle offering, including everything from specification &amp; design to development, testing &amp; management.</p>
        </FeatureCard>
        <FeatureCard icon="write" title="Writing">
          <p>Copywriting, translation &amp; copy checking.</p>
        </FeatureCard>
        <FeatureCard icon="social" title="Social media">
          <p>360 Social media management offering, including everything from content planning &amp; content writing to post design, community management &amp; reporting.</p>
        </FeatureCard>
        <FeatureCard icon="design" title="Design">
          <p>UX/UI design, logo design, prototyping &amp; illustration.</p>
        </FeatureCard>
        <FeatureCard icon="motion" title="Motion graphics">
          <p>Storyboarding, filming, audio recording, audio mixing, post production, 2D/3D animation and more with state of the art equipment &amp; facilities.</p>
        </FeatureCard>
        <FeatureCard icon="pm" title="Project management">
          <p>Dedicated management of every project from start to finish.</p>
        </FeatureCard>
        <FeatureCard icon="ba" title="Business analysis">
          <p>Technical specifications, wireframing &amp; information architecture.</p>
        </FeatureCard>
      </ul>
    );
  }
}

export default Services;

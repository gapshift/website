import React, { Component } from 'react';
import { IconBox } from './';
import Assets from '../constants/Assets';

class LegendBenefits extends Component {
  render() {
    return (
      <ul className="icon-boxes">
        <IconBox icon={Assets.freelanceBenefits.laptop} title="Work remotely">
          <p className="m-text">Whether you choose to work in a local coffee shop or while you’re on vacation in Europe is up to you, as long as you find a place in which you work best. You could even work in a park, at the library, or in your living room while you’re wearing your pajamas.</p>
        </IconBox>
        <IconBox icon={Assets.freelanceBenefits.timeFlex} title="Time flexibility">
          <p className="m-text">Sleeping well is a great benefit for your overall health. That’s why we encourge a rested mind and that you actually work during your most productive hours, whenever that might be, as long as project deadline is still met of course.</p>
        </IconBox>
        <IconBox icon={Assets.freelanceBenefits.rates} title="100% industry rates">
          <p className="m-text">Everybody wants a piece of the cake, but this time we’ll let you have all of it. We pay our freelancers an industry related rate, based on their experience.
          NO MORE profit splitting.</p>
        </IconBox>
        <IconBox icon={Assets.freelanceBenefits.clientNetwork} title="Variety of clients">
          <p className="m-text">You’ll get the chance to work on some exciting clients and brands that could add more experience to your craft, as well as more flavor to your mix of clients.</p>
        </IconBox>
        <IconBox icon={Assets.freelanceBenefits.checklist} title="Project management">
          <p className="m-text">We understand the challenges as a freelancer to wear so many different hats. Managing clients, timelines, billing and more can become a painful task. We’ll take care of all the project management related tasks, so you can focus on what you love most…the work!</p>
        </IconBox>
        <IconBox icon={Assets.freelanceBenefits.rate} title="Ratings">
          <p className="m-text">We’re introducing a rating system that will ensure the quality and continuous improvement of our work and services. Therefore, the better your rating on each job, the more likely you’ll be offered more projects from us.</p>
        </IconBox>
      </ul>
    );
  }
}

export default LegendBenefits;

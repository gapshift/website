import 'babel-polyfill';
import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router } from "react-router-dom";
import App from './App';
import registerServiceWorker from './registerServiceWorker';

// NavBar
ReactDOM.render(

  <Router
  forceRefresh={true}
  >
    <App />
  </Router>,
  document.getElementById('App')
);

registerServiceWorker();
